part of 'home_bloc.dart';

sealed class HomeEvent {
  const HomeEvent();
}

// Загрузка данных с API
final class ApiFetchAsked extends HomeEvent {
  const ApiFetchAsked();
}

// Загрузка данных
final class CandidatesStatusChanged extends HomeEvent {
  const CandidatesStatusChanged(this.status);

  final CandidatesStatus status;
}
