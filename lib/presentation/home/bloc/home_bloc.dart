import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:candidates_app/data/candidate_repository_impl.dart';
import 'package:candidates_app/domain/candidate_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:candidates_app/data/models/candidate_dto.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc({
    required CandidateRepository candidateRepository,
  }): _candidateRepository = candidateRepository,
        super(const HomeState.unknown()){
    on<ApiFetchAsked>(_onApiFetchAsked);
    on<CandidatesStatusChanged>(_onCandidatesStatusChanged);
    _candidatesStatusSubscription = _candidateRepository.status.listen(
          (status) => add(CandidatesStatusChanged(status)),
    );
    _onStorageFetchAsked();
  }
  final CandidateRepository _candidateRepository;
  List<CandidateDTO> candidates = [];
  late StreamSubscription<CandidatesStatus> _candidatesStatusSubscription;

  @override
  Future<void> close() {
    _candidatesStatusSubscription.cancel();
    return super.close();
  }

  Future<void> _onApiFetchAsked(
      ApiFetchAsked event,
      Emitter<HomeState> emit,
      ) async {
    await _candidateRepository.getCandidatesFromAPI();
    candidates = _candidateRepository.candidates;
  }

  Future<void> _onStorageFetchAsked() async {
    await _candidateRepository.getCandidatesFromStorage();
    candidates = _candidateRepository.candidates;
  }

  Future<void> _onCandidatesStatusChanged(
      CandidatesStatusChanged event,
      Emitter<HomeState> emit,
      ) async {
    // print(event.status.toString());
    switch(event.status){
      case CandidatesStatus.unknown:
        return emit(const HomeState.unknown());
      case CandidatesStatus.ready:
        return emit(const HomeState.ready());
      case CandidatesStatus.fetching:
        return emit(const HomeState.fetching());
    }
  }
}