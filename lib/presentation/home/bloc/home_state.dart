part of 'home_bloc.dart';

class HomeState extends Equatable {
  const HomeState._( {
    this.candidates = const [],
    this.status =CandidatesStatus.unknown,
  });

  const HomeState.unknown()
      : this._();

  const HomeState.fetching()
      : this._(status: CandidatesStatus.fetching);

  const HomeState.ready()
      : this._(status: CandidatesStatus.ready);

  final List<CandidateDTO> candidates;
  final CandidatesStatus status;

  @override
  List<Object?> get props => [status];
}
