import 'package:candidates_app/data/models/candidate_dto.dart';
import 'package:candidates_app/presentation/about/view/about_page.dart';
import 'package:candidates_app/presentation/home/bloc/home_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/candidate_repository_impl.dart';

class HomeStub extends StatefulWidget {
  const HomeStub({super.key});

  static Route route() {
    return MaterialPageRoute(builder: (_) => const HomeStub());
  }

  @override
  State<HomeStub> createState() => _HomeState();
}

class _HomeState extends State<HomeStub> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter Candidates App Demo'),
        backgroundColor: Colors.blue,
      ),
      body: BlocBuilder<HomeBloc, HomeState>(
          builder: (BuildContext context, HomeState state) {
            // print(state.status.toString());
            if (state.status == CandidatesStatus.ready) {
              final candidates = context.read<HomeBloc>().candidates;
              List<Widget> candidateCards = List.empty(growable: true);
              for (var candidate in candidates) {
                candidateCards.add(CandidateCard(candidate: candidate));
              }
              return SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: candidateCards,
                ),
              );
            } else {
              return Container(
                alignment: Alignment.center,
                child: const CircularProgressIndicator(),
              );
            }
          }),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async {
          try {
            context.read<HomeBloc>().add(const ApiFetchAsked());
            // print("ButtonPressed");
          } catch (e) {
            print(e);
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text("Что пошло не так при попытке пнуть API"),
            ));
          }
        },
        label: const Text(
          "Запросить данные о людях",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.blueAccent,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

class CandidateCard extends StatelessWidget {
  final CandidateDTO candidate;

  const CandidateCard({super.key, required this.candidate});

  @override
  Widget build(BuildContext context) {
    late Widget ActiveText;
    if(candidate.isActive) {
      ActiveText = const Text("Active", style: TextStyle(color: Colors.green),);
    } else {
      ActiveText = const Text("Non-Active", style: TextStyle(color: Colors.red),);
    }
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: <Widget>[
                const SizedBox(width: 8),
                const Icon(Icons.account_circle),
                const SizedBox(width: 8),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      candidate.name,
                      style: const TextStyle(
                          fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                        child: Text('email: ${candidate.email}')
                    ),
                    ActiveText,
                  ],
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                TextButton(
                  child: const Text('Подробнее'),
                  onPressed: () {
                    if (candidate.isActive) {
                      Navigator.push(
                        context,
                      MaterialPageRoute(builder: (_) => AboutStub(selectedCandidate: candidate))
                    );
                    }
                    else {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text("Невозможно посмотреть информацию о неактивном пользователе"),
                      ));
                    }
                  },
                ),
                const SizedBox(width: 8),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
