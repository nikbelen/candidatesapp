import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../data/models/candidate_dto.dart';
import '../../home/bloc/home_bloc.dart';
import '../../home/view/home_page.dart';

class AboutStub extends StatelessWidget {
  AboutStub({super.key, required this.selectedCandidate});

  final CandidateDTO selectedCandidate;

  Future<void> _makePhoneCall(String phoneNumber) async {
    final Uri launchUri = Uri(
      scheme: 'tel',
      path: phoneNumber,
    );
    await launchUrl(launchUri);
  }

  Future<void> _mailAMessage(String email) async {
    final Uri launchUri = Uri(
      scheme: 'mailto',
      path: email,
    );
    await launchUrl(launchUri);
  }

  Future<void> _openAMap(double lat, double lon) async {
    MapsLauncher.launchCoordinates(lat, lon);
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final candidates = context.read<HomeBloc>().candidates;
    var candidateCards = List.generate(
        selectedCandidate.friends.length,
        (index) => Container(
          width: screenWidth-32 ,
          child: CandidateCard(
              candidate: candidates
                  .where(
                      (element) => element.id == selectedCandidate.friends[index])
                  .first),
        ));
    DateTime date = DateTime.parse(selectedCandidate.registered);
    String favoriteFruit;
    switch (selectedCandidate.favoriteFruit) {
      case "banana":
        favoriteFruit = "А еще пользователь любит 🍌";
        break;
      case "apple":
        favoriteFruit = "А еще пользователь любит 🍏";
        break;
      case "strawberry":
        favoriteFruit = "А еще пользователь любит 🍓";
        break;
      default:
        favoriteFruit =
            "А еще пользователь любит ${selectedCandidate.favoriteFruit}";
    }
    final DateFormat formatter = DateFormat('HH:mm dd.MM.yy');
    Color color;
    switch (selectedCandidate.eyeColor) {
      case "green":
        color = Colors.green;
        break;
      case "blue":
        color = Colors.blue;
        break;
      case "brown":
        color = Colors.brown;
        break;
      default:
        color = Colors.black;
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter Candidates App Demo'),
        backgroundColor: Colors.blue,
      ),
      body: Row(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  selectedCandidate.name,
                  style: const TextStyle(
                      fontSize: 22, fontWeight: FontWeight.bold),
                ),
                BoxedText(
                  screenWidth: screenWidth,
                  widget: Text("Возраст: ${selectedCandidate.age}"),
                ),
                BoxedText(
                  screenWidth: screenWidth,
                  widget: Text("Зарегистрирован: ${formatter.format(date)}"),
                ),
                BoxedText(
                  screenWidth: screenWidth,
                  widget: Text('Компания: ${selectedCandidate.company}'),
                ),
                GestureDetector(
                  onTap: () => _mailAMessage(selectedCandidate.email),
                  child: BoxedText(
                    screenWidth: screenWidth,
                    widget: Text('email: ${selectedCandidate.email}'),
                  ),
                ),
                GestureDetector(
                  onTap: () => _makePhoneCall(selectedCandidate.phone),
                  child: BoxedText(
                    screenWidth: screenWidth,
                    widget: Text('Телефон: ${selectedCandidate.phone}'),
                  ),
                ),
                BoxedText(
                  screenWidth: screenWidth,
                  widget: Text(
                    'Адрес: ${selectedCandidate.address}',
                    softWrap: true,
                  ),
                ),
                GestureDetector(
                  onTap: () => _openAMap(
                      selectedCandidate.latitude, selectedCandidate.longitude),
                  child: BoxedText(
                    screenWidth: screenWidth,
                    widget: Text(
                      'Местоположение: ${selectedCandidate.latitude}°, ${selectedCandidate.longitude}°',
                      softWrap: true,
                    ),
                  ),
                ),
                BoxedText(
                  screenWidth: screenWidth,
                  widget: Text(
                    'Обо мне: ${selectedCandidate.about}',
                    softWrap: true,
                  ),
                ),
                BoxedText(
                  screenWidth: screenWidth,
                  widget: Row(
                    children: [
                      const Text(
                        'Цвет глаз: ',
                      ),
                      Icon(
                        Icons.circle,
                        color: color,
                      ),
                    ],
                  ),
                ),
                BoxedText(
                  screenWidth: screenWidth,
                  widget: Text(favoriteFruit),
                ),
                const Text(
                  "Друзья",
                  style: TextStyle(fontSize: 20),
                ),
                SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: candidateCards,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class BoxedText extends StatelessWidget {
  const BoxedText({super.key, required this.screenWidth, required this.widget});

  final double screenWidth;
  final Widget widget;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: screenWidth,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
        child: widget,
      ),
    );
  }
}
