import 'package:candidates_app/data/candidate_repository_impl.dart';
import 'package:candidates_app/data/models/candidate_dto.dart';
import 'package:candidates_app/data/networking/api_client.dart';
import 'package:candidates_app/data/storage/candidate_storage.dart';
import 'package:candidates_app/data/storage/candidate_storage_hive.dart';
import 'package:candidates_app/domain/candidate_repository.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:hive_flutter/adapters.dart';

import 'app.dart';

GetIt getIt = GetIt.instance;

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(CandidateDTOAdapter());
  await Hive.openBox<CandidateDTO>('candidates');
  getIt.registerSingleton<ApiClient>(ApiClient());
  getIt.registerSingleton<CandidateStorage>(HiveCandidateStorage());
  getIt.registerSingleton<CandidateRepository>(CandidateRepositoryImplementation());
  runApp(const App());
}