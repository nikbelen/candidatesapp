import 'package:candidates_app/data/candidate_repository_impl.dart';
import 'package:candidates_app/domain/candidate_repository.dart';
import 'package:candidates_app/presentation/home/bloc/home_bloc.dart';
import 'package:candidates_app/presentation/home/view/home_page.dart';
import 'package:candidates_app/presentation/splash/view/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  late final CandidateRepository _candidateRepository;

  @override
  void initState() {
    super.initState();
    _candidateRepository = (GetIt.instance.isRegistered<CandidateRepository>()
        ? GetIt.instance.get<CandidateRepository>()
        : null)!;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: _candidateRepository,
      child: BlocProvider(
        create: (_) =>
            HomeBloc(
              candidateRepository: _candidateRepository,
            ),
        child: const AppView(),
      ),
    );
  }
}

class AppView extends StatefulWidget {
  const AppView({super.key});

  @override
  State<AppView> createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState get _navigator => _navigatorKey.currentState!;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Candidates App Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        useMaterial3: true,
      ),
      navigatorKey: _navigatorKey,
      builder: (context, child) {
        return BlocListener<HomeBloc, HomeState>(
          listener: (context, state) {
            print(_navigator.toString());
            print(state.toString());
            switch(state.status){
              case CandidatesStatus.unknown:
                break;
              case CandidatesStatus.fetching:
                break;
              case CandidatesStatus.ready:
                _navigator.pushAndRemoveUntil<void>(
                  HomeStub.route(),
                      (route) => false,
                );
            }
          },
          child: child,
        );
      },
      onGenerateRoute: (_) => SplashPage.route(),
    );
  }
}