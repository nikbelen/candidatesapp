import 'package:candidates_app/data/models/candidate_dto.dart';
import 'package:dio/dio.dart';

class ApiClient{
  final Dio _dio = Dio();

  static final ApiClient _instance = ApiClient._internal();

  // Constructors
  ApiClient._internal();

  factory ApiClient() => _instance;

  Future<List<CandidateDTO>> getPeopleData() async {
    late List<CandidateDTO> peopleList;
    try {
      // 404
      final resp = await _dio.get('https://firebasestorage.googleapis.com/v0/b/candidates--questionnaire.appspot.com/o/users.json?alt=media&token=e3672c23-b1a5-4ca7-bb77-b6580d75810c');
      // print(resp.data);
      peopleList = parsePeopleJson(resp.data);
      return peopleList;
    } on DioException catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      final response = e.response;
      if (response != null) {
        print(response.data);
        print(response.headers);
        print(response.requestOptions);
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        print(e.requestOptions);
        print(e.message);
      }
    }
    return List.empty();
  }

  List<CandidateDTO> parsePeopleJson(dynamic json){
    List<CandidateDTO> peopleList = List.empty(growable: true);
    for (var a in json) {
      peopleList.add(CandidateDTO.fromJson(a));
    }
    return peopleList;
  }
}