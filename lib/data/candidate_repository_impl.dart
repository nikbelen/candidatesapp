import 'dart:async';

import 'package:candidates_app/data/networking/api_client.dart';
import 'package:candidates_app/data/storage/candidate_storage.dart';
import 'package:candidates_app/domain/candidate_repository.dart';
import 'package:get_it/get_it.dart';

enum CandidatesStatus { unknown, fetching, ready }

class CandidateRepositoryImplementation extends CandidateRepository {
  final _controller = StreamController<CandidatesStatus>();
  final _api = GetIt.instance.isRegistered<ApiClient>()
      ? GetIt.instance.get<ApiClient>()
      : null;
  final _storage = GetIt.instance.isRegistered<CandidateStorage>()
      ? GetIt.instance.get<CandidateStorage>()
      : null;

  @override
  Stream<CandidatesStatus> get status async* {
    await Future<void>.delayed(const Duration(seconds: 1));
    yield CandidatesStatus.fetching;
    yield* _controller.stream;
  }

  @override
  Future<void> getCandidatesFromAPI() async {
    final candidatesList = await _api?.getPeopleData();
    if (candidatesList!.isNotEmpty) {
      candidates = candidatesList;
      _storage?.deleteAllCandidates();
      _storage?.insertAllCandidates(candidatesList);
      _controller.add(CandidatesStatus.ready);
    } else {
      _controller.add(CandidatesStatus.unknown);
    }
  }

  @override
  Future<void> getCandidatesFromStorage() async {
    final candidatesList = await _storage?.getAllCandidates();
    if (candidatesList!.isNotEmpty) {
      candidates = candidatesList;
      _controller.add(CandidatesStatus.ready);
    } else {
      _controller.add(CandidatesStatus.unknown);
      await getCandidatesFromAPI();
    }
  }

  void dispose() => _controller.close();
}
