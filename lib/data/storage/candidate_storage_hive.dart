import 'package:candidates_app/data/models/candidate_dto.dart';
import 'package:candidates_app/data/storage/candidate_storage.dart';
import 'package:hive/hive.dart';

class HiveCandidateStorage extends CandidateStorage {
  final candidateBox = Hive.box<CandidateDTO>('candidates');

  @override
  Future<void> deleteAllCandidates() async{
    await candidateBox.clear();
  }

  @override
  Future<List<CandidateDTO>> getAllCandidates() async {
    return candidateBox.values.toList();
  }

  @override
  Future<CandidateDTO?> getCandidateById(int id) async{
    return candidateBox.get(id);
  }

  @override
  Future<void> insertAllCandidates(List<CandidateDTO> candidates) async{
    for(var candidate in candidates){
      await candidateBox.put(candidate.id, candidate);
    }
  }

  @override
  Future<void> insertCandidate(CandidateDTO candidate) async{
    await candidateBox.put(candidate.id, candidate);
  }
}
