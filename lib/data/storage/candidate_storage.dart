import 'package:candidates_app/data/models/candidate_dto.dart';

abstract class CandidateStorage {
  Future<List<CandidateDTO>> getAllCandidates();

  Future<CandidateDTO?> getCandidateById(int id);

  Future<void> deleteAllCandidates();

  Future<void> insertCandidate(CandidateDTO candidate);

  Future<void> insertAllCandidates(List<CandidateDTO> candidates);
}