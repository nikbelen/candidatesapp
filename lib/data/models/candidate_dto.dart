import 'package:hive/hive.dart';

part 'candidate_dto.g.dart';

@HiveType(typeId: 0)
class CandidateDTO extends HiveObject {
  @HiveField(0)
  final int id;
  @HiveField(1)
  final String guid;
  @HiveField(2)
  final bool isActive;
  @HiveField(3)
  final String balance;
  @HiveField(4)
  final int age;
  @HiveField(5)
  final String eyeColor;
  @HiveField(6)
  final String name;
  @HiveField(7)
  final String gender;
  @HiveField(8)
  final String company;
  @HiveField(9)
  final String email;
  @HiveField(10)
  final String phone;
  @HiveField(11)
  final String address;
  @HiveField(12)
  final String about;
  @HiveField(13)
  final String registered;
  @HiveField(14)
  final double latitude;
  @HiveField(15)
  final double longitude;
  @HiveField(16)
  final List<String> tags;
  @HiveField(17)
  final List<int> friends;
  @HiveField(18)
  final String favoriteFruit;

  CandidateDTO(
      this.name,
      this.email,
      this.id,
      this.guid,
      this.isActive,
      this.balance,
      this.age,
      this.eyeColor,
      this.gender,
      this.company,
      this.phone,
      this.address,
      this.about,
      this.registered,
      this.latitude,
      this.longitude,
      this.tags,
      this.friends,
      this.favoriteFruit);

  CandidateDTO.fromJson(
    Map<String, dynamic> json,
  )   : id = json['id'] as int,
        guid = json['guid'] as String,
        isActive = json['isActive'] as bool,
        balance = json['balance'] as String,
        age = json['age'] as int,
        eyeColor = json['eyeColor'] as String,
        name = json['name'] as String,
        gender = json['gender'] as String,
        company = json['company'] as String,
        email = json['email'] as String,
        phone = json['phone'] as String,
        address = json['address'] as String,
        about = json['about'] as String,
        registered = json['registered'] as String,
        latitude = json['latitude'] as double,
        longitude = json['longitude'] as double,
        tags = (json['tags'] as List).map((item) => item as String).toList(),
        friends = (json['friends'] as List<dynamic>)
            .map((item) => item as Map<String, dynamic>)
            .map((item) => item.values.first as int)
            .toList(),
        favoriteFruit = json['favoriteFruit'] as String;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'email': email,
      };
}

// import 'dart:convert';
// import 'dart:io';
//
// Future<List<CandidateDTO>> readJsonFile(String filePath) async {
//   var input = await File(filePath).readAsString();
//   List<CandidateDTO> aboba = List.empty(growable: true);
//   var map = jsonDecode(input);
//   // print(map);
//   for (var a in map) {
//     aboba.add(CandidateDTO.fromJson(a));
//   }
//   return aboba;
// }
//
// Future<void> main(List<String> arguments) async {
//   exitCode = 0; // Presume success
//   var abobas = await readJsonFile(
//       'C:\\Users\\shado\\AndroidStudioProjects\\candidates_app\\assets\\users.json');
//   for (var aboba in abobas) {
//     print(aboba.id);
//     for (var i in aboba.friends) {
//       print(i);
//     }
//     print("");
//   }
// }
