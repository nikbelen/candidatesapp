// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'candidate_dto.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CandidateDTOAdapter extends TypeAdapter<CandidateDTO> {
  @override
  final int typeId = 0;

  @override
  CandidateDTO read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CandidateDTO(
      fields[6] as String,
      fields[9] as String,
      fields[0] as int,
      fields[1] as String,
      fields[2] as bool,
      fields[3] as String,
      fields[4] as int,
      fields[5] as String,
      fields[7] as String,
      fields[8] as String,
      fields[10] as String,
      fields[11] as String,
      fields[12] as String,
      fields[13] as String,
      fields[14] as double,
      fields[15] as double,
      (fields[16] as List).cast<String>(),
      (fields[17] as List).cast<int>(),
      fields[18] as String,
    );
  }

  @override
  void write(BinaryWriter writer, CandidateDTO obj) {
    writer
      ..writeByte(19)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.guid)
      ..writeByte(2)
      ..write(obj.isActive)
      ..writeByte(3)
      ..write(obj.balance)
      ..writeByte(4)
      ..write(obj.age)
      ..writeByte(5)
      ..write(obj.eyeColor)
      ..writeByte(6)
      ..write(obj.name)
      ..writeByte(7)
      ..write(obj.gender)
      ..writeByte(8)
      ..write(obj.company)
      ..writeByte(9)
      ..write(obj.email)
      ..writeByte(10)
      ..write(obj.phone)
      ..writeByte(11)
      ..write(obj.address)
      ..writeByte(12)
      ..write(obj.about)
      ..writeByte(13)
      ..write(obj.registered)
      ..writeByte(14)
      ..write(obj.latitude)
      ..writeByte(15)
      ..write(obj.longitude)
      ..writeByte(16)
      ..write(obj.tags)
      ..writeByte(17)
      ..write(obj.friends)
      ..writeByte(18)
      ..write(obj.favoriteFruit);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CandidateDTOAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
