import '../data/models/candidate_dto.dart';

abstract class CandidateRepository {
  List<CandidateDTO> candidates = List.empty(growable: true);

  get status => null;

  Future<void> getCandidatesFromAPI();

  Future<void> getCandidatesFromStorage();
}